# parallel-final-project
Final project of Parallel &amp; Distributed Computing. Based on CUDA

**To use**

`git clone https://gitlab.com/Petrides/parallel-computing-final-project.git`

`cd Parallel-Computing-final-project`

`make`

`./cuda-triangles-exe`



**Parameters**

The parameters are in main.cu, in the very beginning of main()

Namely: thread Multiplier, block Multiplier, graph filepath
#ifndef TEST_H
#define TEST_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <float.h>
#include <stdint.h>

#include "utils.h"
#include "cudaTriangles.h"


#define EPS .005
extern int tests_total;
extern int tests_fail;
#define TEST(EX) do { ++tests_total; if(!(EX)) {\
    fprintf(stderr, "failed: [%s] testing [%s] in %s, line %d\n", __FUNCTION__, #EX, __FILE__, __LINE__); \
    ++tests_fail; }else{fprintf(stderr, "passed: [%s] testing [%s] in %s, line %d\n", __FUNCTION__, #EX, __FILE__, __LINE__);}} while (0)


void test();

int64_t serial_triangleSum(int *allRowsArray, int *nzeCummus, pair *pairs_rm, int nze, int N);

#endif

#include "test.h"

int tests_total = 0;
int tests_fail = 0;


int close(float a, float b)
{
    return a-EPS<b && b<a+EPS;
}

void test()
{
    printf("%d tests, %d passed, %d failed\n", tests_total, tests_total-tests_fail, tests_fail);
}

int64_t serial_triangleSum(int *allRowsArray, int *nzeCummus, pair *pairs_rm, int nze, int N){
    int64_t sum = 0;
    struct pair currPair;
    int col;
    int row;
    int tempSum;
    
    for(int i=0; i<nze;i++){
        currPair = pairs_rm[i];
        col = currPair.col;
        row = currPair.row;

        tempSum = commonElementCount(&allRowsArray[nzeCummus[row-1]] , allRowsArray[nzeCummus[row-1]] , &allRowsArray[nzeCummus[col-1]] , allRowsArray[nzeCummus[col-1]] , row , col); 
        sum += tempSum;

    }

    return sum;
}

